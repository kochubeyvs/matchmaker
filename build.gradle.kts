val ktorVersion: String by project
val coroutinesVersion: String by project
val serializationVersion: String by project
val konveyorVersion: String by project
val logbackVersion: String by project
val slf4jVersion: String by project
val konformVersion: String by project
val valiktorVersion: String by project

plugins {
    application
    kotlin("jvm")
    id("org.jetbrains.kotlin.plugin.serialization")
}

group = "ru.vsko.matchmaker"
version = "0.1.1"

application {
    mainClass.set("ru.vsko.matchmaker.ApplicationKt")
}

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-serialization:$ktorVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
    implementation("codes.spectrum:konveyor:$konveyorVersion")
    implementation("org.valiktor:valiktor-core:$valiktorVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")

    testImplementation(kotlin("test-junit"))
}
