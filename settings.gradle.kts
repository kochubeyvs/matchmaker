rootProject.name = "matchmaker"

pluginManagement {
    val kotlinVersion: String by settings

    plugins {
        kotlin("jvm") version kotlinVersion apply false
        id("org.jetbrains.kotlin.plugin.serialization") version kotlinVersion apply false
    }
}