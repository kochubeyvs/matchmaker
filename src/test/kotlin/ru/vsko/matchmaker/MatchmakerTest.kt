package ru.vsko.matchmaker

import org.junit.Assert.assertTrue
import org.junit.Test
import ru.vsko.matchmaker.configs.MatchmakerConfig
import ru.vsko.matchmaker.logic.ChainFacade
import ru.vsko.matchmaker.models.dto.input.UserInputDto
import ru.vsko.matchmaker.repo.MatchmakerInMemoryRepo
import java.time.Duration
import java.time.Instant
import java.util.*
import kotlin.random.Random

class MatchmakerTest {

    @Test
    fun `group successfully formed`() {
        val matchmakerConfig = MatchmakerConfig(
            groupSize = 4,
            groupSolverSkillRange = 0.05,
            groupSolverLatencyRange = 0.1,
            groupSolverHelpEvery = 10000,
            groupSolverInvokePeriod = 100
        )

        val users = listOf(
            UserInputDto(name = "name1", skill = "100.0", latency = "100.0"),
            UserInputDto(name = "name2", skill = "100.0", latency = "100.0"),
            UserInputDto(name = "name3", skill = "100.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "100.0"),
        )

        val chainFacade = ChainFacade(
            matchmakerConfig = matchmakerConfig,
            matchmakerRepo = MatchmakerInMemoryRepo()
        )

        repeat(3) {
            val chainContext = chainFacade.defaultChainContext().apply { userInputDto = users[it] }
            chainFacade.runUserChain(chainContext)
            assertTrue(chainContext.formedGroup.isEmpty())
        }

        val chainContext4 = chainFacade.defaultChainContext().apply { userInputDto = users[3] }
        chainFacade.runUserChain(chainContext4)
        assertTrue(4 == chainContext4.formedGroup.size)
    }

    @Test
    fun `double entrance handled successfully`() {
        val matchmakerConfig = MatchmakerConfig(
            groupSize = 4,
            groupSolverSkillRange = 0.05,
            groupSolverLatencyRange = 0.1,
            groupSolverHelpEvery = 10000,
            groupSolverInvokePeriod = 100
        )
        val users = listOf(
            UserInputDto(name = "name1", skill = "100.0", latency = "100.0"),
            UserInputDto(name = "name2", skill = "100.0", latency = "120.0"),
            UserInputDto(name = "name3", skill = "100.0", latency = "140.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "160.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "150.0"),
            UserInputDto(name = "name6", skill = "100.0", latency = "150.0"),
            UserInputDto(name = "name7", skill = "100.0", latency = "150.0"),
            UserInputDto(name = "name8", skill = "100.0", latency = "150.0")
        )

        val chainFacade = ChainFacade(
            matchmakerConfig = matchmakerConfig,
            matchmakerRepo = MatchmakerInMemoryRepo()
        )

        repeat(6) {
            val chainContext = chainFacade.defaultChainContext().apply { userInputDto = users[it] }
            chainFacade.runUserChain(chainContext)
            assertTrue(chainContext.formedGroup.isEmpty())
        }

        val chainContext1 = chainFacade.defaultChainContext().apply { userInputDto = users[6] }
        chainFacade.runUserChain(chainContext1)
        assertTrue(4 == chainContext1.formedGroup.size)

        val chainContext2 = chainFacade.defaultChainContext().apply { userInputDto = users[7] }
        chainFacade.runUserChain(chainContext2)
        assertTrue(chainContext2.formedGroup.isEmpty())
    }

    @Test
    fun `group not formed due to different latency`() {
        val matchmakerConfig = MatchmakerConfig(
            groupSize = 4,
            groupSolverSkillRange = 0.05,
            groupSolverLatencyRange = 0.1,
            groupSolverHelpEvery = 10000,
            groupSolverInvokePeriod = 100
        )

        val users = listOf(
            UserInputDto(name = "name1", skill = "100.0", latency = "100.0"),
            UserInputDto(name = "name2", skill = "100.0", latency = "110.0"),
            UserInputDto(name = "name3", skill = "100.0", latency = "120.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "130.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "140.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "160.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "180.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "200.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "220.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "240.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "260.0"),
            UserInputDto(name = "name4", skill = "100.0", latency = "280.0"),
        )

        val chainFacade = ChainFacade(
            matchmakerConfig = matchmakerConfig,
            matchmakerRepo = MatchmakerInMemoryRepo()
        )

        repeat(12) {
            val chainContext = chainFacade.defaultChainContext().apply { userInputDto = users[it] }
            chainFacade.runUserChain(chainContext)
            assertTrue(chainContext.formedGroup.isEmpty())
        }
    }

    @Test
    fun `group not formed due to different skill`() {
        val matchmakerConfig = MatchmakerConfig(
            groupSize = 4,
            groupSolverSkillRange = 0.05,
            groupSolverLatencyRange = 0.1,
            groupSolverHelpEvery = 10000,
            groupSolverInvokePeriod = 100
        )

        val users = listOf(
            UserInputDto(name = "name1", skill = "100.0", latency = "100.0"),
            UserInputDto(name = "name2", skill = "105.0", latency = "100.0"),
            UserInputDto(name = "name3", skill = "110.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "115.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "120.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "130.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "140.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "150.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "160.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "170.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "180.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "190.0", latency = "100.0"),
        )

        val chainFacade = ChainFacade(
            matchmakerConfig = matchmakerConfig,
            matchmakerRepo = MatchmakerInMemoryRepo()
        )

        repeat(12) {
            val chainContext = chainFacade.defaultChainContext().apply { userInputDto = users[it] }
            chainFacade.runUserChain(chainContext)
            assertTrue(chainContext.formedGroup.isEmpty())
        }
    }

    @Test
    fun `group not formed due to different skill even with helper`() {
        val matchmakerConfig = MatchmakerConfig(
            groupSize = 4,
            groupSolverSkillRange = 0.05,
            groupSolverLatencyRange = 0.1,
            groupSolverHelpEvery = 10000,
            groupSolverInvokePeriod = 100
        )

        val users = listOf(
            UserInputDto(name = "name1", skill = "100.0", latency = "100.0"),
            UserInputDto(name = "name2", skill = "110.0", latency = "100.0"),
            UserInputDto(name = "name3", skill = "120.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "130.0", latency = "100.0"),
        )

        val chainFacade = ChainFacade(
            matchmakerConfig = matchmakerConfig,
            matchmakerRepo = MatchmakerInMemoryRepo()
        )

        repeat(4) {
            val chainContext = chainFacade.defaultChainContext().apply { userInputDto = users[it] }
            chainFacade.runUserChain(chainContext)
            assertTrue(chainContext.formedGroup.isEmpty())
        }

        val now = Instant.now()
        val chainContext = chainFacade.defaultChainContext(now.plusMillis(10001))
        chainFacade.runHelperChain(chainContext)
        assertTrue(chainContext.formedGroup.isEmpty())
    }

    @Test
    fun `group formed instead of different skill because of helper`() {
        val matchmakerConfig = MatchmakerConfig(
            groupSize = 4,
            groupSolverSkillRange = 0.05,
            groupSolverLatencyRange = 0.1,
            groupSolverHelpEvery = 10000,
            groupSolverInvokePeriod = 100
        )

        val users = listOf(
            UserInputDto(name = "name1", skill = "100.0", latency = "100.0"),
            UserInputDto(name = "name2", skill = "110.0", latency = "100.0"),
            UserInputDto(name = "name3", skill = "120.0", latency = "100.0"),
            UserInputDto(name = "name4", skill = "130.0", latency = "100.0"),
        )

        val chainFacade = ChainFacade(
            matchmakerConfig = matchmakerConfig,
            matchmakerRepo = MatchmakerInMemoryRepo()
        )

        repeat(4) {
            val chainContext = chainFacade.defaultChainContext().apply { userInputDto = users[it] }
            chainFacade.runUserChain(chainContext)
            assertTrue(chainContext.formedGroup.isEmpty())
        }

        val now = Instant.now()
        val chainContext1 = chainFacade.defaultChainContext(now.plusMillis(10001))
        chainFacade.runHelperChain(chainContext1)
        assertTrue(chainContext1.formedGroup.isEmpty())

        val chainContext2 = chainFacade.defaultChainContext(now.plusMillis(20001))
        chainFacade.runHelperChain(chainContext2)
        assertTrue(4 == chainContext2.formedGroup.size)
    }

    @Test
    fun `stress test`() {
        val matchmakerConfig = MatchmakerConfig(
            groupSize = 30,
            groupSolverSkillRange = 0.05,
            groupSolverLatencyRange = 0.1,
            groupSolverHelpEvery = 10000,
            groupSolverInvokePeriod = 100
        )

        val chainFacade = ChainFacade(
            matchmakerConfig = matchmakerConfig,
            matchmakerRepo = MatchmakerInMemoryRepo()
        )

        repeat(10000) {
            val chainContext = chainFacade.defaultChainContext().apply {
                userInputDto = UserInputDto(
                    name = UUID.randomUUID().toString().substring(0, 20).replace("-", "_"),
                    skill = Random.nextDouble(0.0, 1000.0).toString(),
                    latency = Random.nextDouble(0.0, 1000.0).toString()
                )
            }
            chainFacade.runUserChain(chainContext)
            if (it != 0) {
                val fasterThan10ms = Duration.between(chainContext.startTime, Instant.now()).toMillis() < 10
                assertTrue(fasterThan10ms)
            }
        }
    }
}