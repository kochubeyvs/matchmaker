package ru.vsko.matchmaker

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.serialization.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.vsko.matchmaker.extensions.initConfig
import ru.vsko.matchmaker.logic.ChainFacade
import ru.vsko.matchmaker.models.dto.input.UserInputDto
import ru.vsko.matchmaker.repo.MatchmakerInMemoryRepo

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module() {
    install(Routing)
    install(ContentNegotiation) { json() }
    install(CORS) {
        method(HttpMethod.Post)
        anyHost()
    }

    val matchmakerConfig = initConfig()
    val chainFacade = ChainFacade(
        matchmakerConfig = matchmakerConfig,
        matchmakerRepo = MatchmakerInMemoryRepo()
    )

    routing {
        post("/users") {
            val queryParams = call.request.queryParameters
            val userInputDto = UserInputDto(
                name = queryParams["name"],
                skill = queryParams["skill"],
                latency = queryParams["latency"]
            )
            val chainContext = chainFacade.defaultChainContext().also { it.userInputDto = userInputDto }
            chainFacade.runUserChain(chainContext)
            call.respond(
                status = chainContext.httpResponse.status,
                message = chainContext.httpResponse.data ?: ""
            )
        }
    }

    launch {
        while (true) {
            delay(matchmakerConfig.groupSolverInvokePeriod)
            val chainContext = chainFacade.defaultChainContext()
            chainFacade.runHelperChain(chainContext)
        }
    }
}