package ru.vsko.matchmaker.models.inner

import java.time.Instant
import kotlin.math.abs
import kotlin.math.pow
import kotlin.reflect.KProperty1

data class UserInner(
    val name: String,
    val skill: Double,
    val latency: Double,
    val joinPoolTime: Instant,
    val helpingPower: Int
) {
    private fun absPropDiff(prop: KProperty1<UserInner, Double>, user: UserInner) = abs(prop.get(this) - prop.get(user))

    private fun propRangeWithRate(prop: KProperty1<UserInner, Double>, rate: Double) = prop.get(this) * ((rate + 1).pow(helpingPower) - 1)

    private fun hasIntersectionByProp(prop: KProperty1<UserInner, Double>, rate: Double, user: UserInner) =
        absPropDiff(prop, user) < propRangeWithRate(prop, rate) + user.propRangeWithRate(prop, rate)

    fun hasIntersectionsBySkillAndLatency(user: UserInner, skillIncRate: Double, latencyIncRate: Double): Boolean {
        return hasIntersectionByProp(UserInner::skill, skillIncRate, user)
                && hasIntersectionByProp(UserInner::latency, latencyIncRate, user)
    }
}