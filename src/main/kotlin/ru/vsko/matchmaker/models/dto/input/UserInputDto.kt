package ru.vsko.matchmaker.models.dto.input

data class UserInputDto(
    val name: String? = null,
    val skill: String? = null,
    val latency: String? = null
)
