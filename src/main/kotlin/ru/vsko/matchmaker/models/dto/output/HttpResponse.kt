package ru.vsko.matchmaker.models.dto.output

import io.ktor.http.*
import kotlinx.serialization.Serializable

data class HttpResponse(
    val status: HttpStatusCode,
    val data: ResponseData? = null
)

@Serializable
data class ResponseData(
    val message: String?,
    val errors: List<ResponseError>?
)

@Serializable
data class ResponseError(
    val message: String?,
    val level: ResponseErrorLevel?
)

@Serializable
enum class ResponseErrorLevel {
    INFO, WARNING, ERROR
}
