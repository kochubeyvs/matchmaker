package ru.vsko.matchmaker.configs

data class MatchmakerConfig(
    val groupSize: Int,
    val groupSolverSkillRange: Double,
    val groupSolverLatencyRange: Double,
    val groupSolverHelpEvery: Long,
    val groupSolverInvokePeriod: Long,
)

