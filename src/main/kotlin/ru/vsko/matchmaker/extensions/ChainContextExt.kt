package ru.vsko.matchmaker.extensions

import ru.vsko.matchmaker.logic.ChainContext
import ru.vsko.matchmaker.logic.ChainError
import ru.vsko.matchmaker.logic.ChainErrorLevel
import ru.vsko.matchmaker.models.inner.UserInner

fun ChainContext.addError(message: String? = null, level: ChainErrorLevel? = null, ex: Throwable? = null) {
    val error = ChainError(
        message = message ?: ex?.message ?: "",
        level = level ?: ChainErrorLevel.ERROR,
        stackTrace = ex?.stackTraceToString()
    )
    chainErrors.add(error)
}

fun ChainContext.formGroupFor(user: UserInner, users: List<UserInner>) {
    val newGroup = mutableListOf(user)
    users.forEach { userInPool ->
        if (user != userInPool) {
            val userMatchGroup = newGroup.all { newGroupUser ->
                userInPool.hasIntersectionsBySkillAndLatency(
                    user = newGroupUser,
                    skillIncRate = matchmakerConfig.groupSolverSkillRange,
                    latencyIncRate = matchmakerConfig.groupSolverLatencyRange
                )
            }
            if (userMatchGroup) {
                newGroup.add(userInPool)
                if (newGroup.size == matchmakerConfig.groupSize) {
                    formedGroup = newGroup
                    return
                }
            }
        }
    }
}