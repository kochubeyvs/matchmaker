package ru.vsko.matchmaker.extensions

import io.ktor.application.*
import io.ktor.config.*
import ru.vsko.matchmaker.configs.MatchmakerConfig

fun Application.initConfig(): MatchmakerConfig = MatchmakerConfig(
    groupSize = getIntProperty("matchmaker.group.size"),
    groupSolverSkillRange = getDoubleProperty("matchmaker.group.solver.skill.range"),
    groupSolverLatencyRange = getDoubleProperty("matchmaker.group.solver.latency.range"),
    groupSolverHelpEvery = getLongProperty("matchmaker.group.solver.help.every"),
    groupSolverInvokePeriod = getLongProperty("matchmaker.group.solver.invoke.period"),
)

fun Application.getStringProperty(propertyName: String): String = environment.config.property(propertyName).getString()

fun Application.getIntProperty(propertyName: String): Int = getStringProperty(propertyName).toIntOrNull()
    ?: throw ApplicationConfigurationException("Property $propertyName can't be casted to Int")

fun Application.getLongProperty(propertyName: String): Long = getStringProperty(propertyName).toLongOrNull()
    ?: throw ApplicationConfigurationException("Property $propertyName can't be casted to Long")

fun Application.getDoubleProperty(propertyName: String): Double = getStringProperty(propertyName).toDoubleOrNull()
    ?: throw ApplicationConfigurationException("Property $propertyName can't be casted to Double")