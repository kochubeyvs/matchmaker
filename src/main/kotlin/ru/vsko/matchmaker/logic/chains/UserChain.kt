package ru.vsko.matchmaker.logic.chains

import codes.spectrum.konveyor.DefaultKonveyorEnvironment
import codes.spectrum.konveyor.IKonveyorEnvironment
import codes.spectrum.konveyor.konveyor
import ru.vsko.matchmaker.logic.ChainContext
import ru.vsko.matchmaker.logic.handlers.FormGroupHandler
import ru.vsko.matchmaker.logic.handlers.OutputGroupHandler
import ru.vsko.matchmaker.logic.handlers.PrepareResponseHandler
import ru.vsko.matchmaker.logic.handlers.ValidateUserInputDtoHandler

object UserChain {

    suspend fun exec(chainContext: ChainContext) {
        exec(chainContext, DefaultKonveyorEnvironment)
    }

    suspend fun exec(chainContext: ChainContext, env: IKonveyorEnvironment) {
        konveyor.exec(chainContext, env)
    }

    private val konveyor = konveyor<ChainContext> {
        +ValidateUserInputDtoHandler
        +FormGroupHandler
        +OutputGroupHandler
        +PrepareResponseHandler
    }
}
