package ru.vsko.matchmaker.logic.chains

import codes.spectrum.konveyor.DefaultKonveyorEnvironment
import codes.spectrum.konveyor.IKonveyorEnvironment
import codes.spectrum.konveyor.konveyor
import ru.vsko.matchmaker.logic.ChainContext
import ru.vsko.matchmaker.logic.handlers.IncHelperPowerHandler
import ru.vsko.matchmaker.logic.handlers.OutputGroupHandler

object HelperChain {

    suspend fun exec(chainContext: ChainContext) {
        exec(chainContext, DefaultKonveyorEnvironment)
    }

    suspend fun exec(chainContext: ChainContext, env: IKonveyorEnvironment) {
        konveyor.exec(chainContext, env)
    }

    private val konveyor = konveyor<ChainContext> {
        +IncHelperPowerHandler
        +OutputGroupHandler
    }
}
