package ru.vsko.matchmaker.logic

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import ru.vsko.matchmaker.configs.MatchmakerConfig
import ru.vsko.matchmaker.logic.chains.HelperChain
import ru.vsko.matchmaker.logic.chains.UserChain
import ru.vsko.matchmaker.repo.IMatchmakerRepo
import java.time.Instant

class ChainFacade(
    private val matchmakerConfig: MatchmakerConfig,
    private val matchmakerRepo: IMatchmakerRepo
) {
    private val mutex = Mutex()

    fun defaultChainContext(startTime: Instant = Instant.now()) = ChainContext(
        matchmakerConfig = matchmakerConfig,
        matchmakerRepo = matchmakerRepo,
        startTime = startTime
    )

    fun runUserChain(chainContext: ChainContext) = runBlocking {
        mutex.withLock {
            UserChain.exec(chainContext)
        }
    }

    fun runHelperChain(chainContext: ChainContext) = runBlocking {
        mutex.withLock {
            HelperChain.exec(chainContext)
        }
    }
}
