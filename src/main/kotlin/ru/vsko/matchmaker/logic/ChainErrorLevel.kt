package ru.vsko.matchmaker.logic

enum class ChainErrorLevel {
    INFO,
    WARNING,
    ERROR;
}
