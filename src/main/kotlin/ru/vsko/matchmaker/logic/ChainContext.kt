package ru.vsko.matchmaker.logic

import ru.vsko.matchmaker.configs.MatchmakerConfig
import ru.vsko.matchmaker.models.dto.input.UserInputDto
import ru.vsko.matchmaker.models.dto.output.HttpResponse
import ru.vsko.matchmaker.models.inner.UserInner
import ru.vsko.matchmaker.repo.IMatchmakerRepo
import java.time.Instant

data class ChainContext(
    val matchmakerConfig: MatchmakerConfig,
    val matchmakerRepo: IMatchmakerRepo,
    val startTime: Instant
) {
    lateinit var userInputDto: UserInputDto
    lateinit var newUser: UserInner
    lateinit var httpResponse: HttpResponse

    var formedGroup: List<UserInner> = emptyList()

    var chainStatus: ChainStatus = ChainStatus.RUNNING
    var chainErrors: MutableList<ChainError> = mutableListOf()
}
