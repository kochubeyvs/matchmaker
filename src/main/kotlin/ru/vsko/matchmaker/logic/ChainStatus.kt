package ru.vsko.matchmaker.logic

enum class ChainStatus {
    RUNNING, FAILING
}