package ru.vsko.matchmaker.logic

data class ChainError(
    val message: String,
    val level: ChainErrorLevel,
    val stackTrace: String? = null
)
