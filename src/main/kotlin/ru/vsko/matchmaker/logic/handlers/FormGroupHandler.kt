package ru.vsko.matchmaker.logic.handlers

import codes.spectrum.konveyor.IKonveyorEnvironment
import codes.spectrum.konveyor.IKonveyorHandler
import ru.vsko.matchmaker.extensions.formGroupFor
import ru.vsko.matchmaker.logic.ChainContext
import ru.vsko.matchmaker.logic.ChainStatus


object FormGroupHandler : IKonveyorHandler<ChainContext> {

    override suspend fun exec(context: ChainContext, env: IKonveyorEnvironment) {
        with(context) {
            val users = matchmakerRepo.getUsersSortedByJoinTime()
            formGroupFor(newUser, users)
            if (formedGroup.isEmpty()) {
                matchmakerRepo.putUser(newUser)
            } else {
                matchmakerRepo.removeUsersByNames(formedGroup.map { it.name })
            }
        }
    }

    override fun match(context: ChainContext, env: IKonveyorEnvironment): Boolean {
        return context.chainStatus == ChainStatus.RUNNING
    }
}