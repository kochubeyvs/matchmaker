package ru.vsko.matchmaker.logic.handlers

import codes.spectrum.konveyor.IKonveyorEnvironment
import codes.spectrum.konveyor.IKonveyorHandler
import ru.vsko.matchmaker.extensions.formGroupFor
import ru.vsko.matchmaker.logic.ChainContext
import ru.vsko.matchmaker.logic.ChainStatus
import ru.vsko.matchmaker.models.inner.UserInner
import java.time.Duration


object IncHelperPowerHandler : IKonveyorHandler<ChainContext> {

    override suspend fun exec(context: ChainContext, env: IKonveyorEnvironment) {
        with(context) {
            val updatedUsers = mutableListOf<UserInner>()
            val users = matchmakerRepo.getUsersSortedByJoinTime().map { user ->
                val helpingPower = 1 + (Duration.between(user.joinPoolTime, startTime)
                    .toMillis() / matchmakerConfig.groupSolverHelpEvery).toInt()
                if (user.helpingPower != helpingPower) {
                    val updatedUser = user.copy(helpingPower = helpingPower)
                    updatedUsers.add(updatedUser)
                    updatedUser
                } else {
                    user
                }
            }
            users.forEach { user ->
                formGroupFor(user, users)
                if (formedGroup.isNotEmpty()) return@forEach
            }
            matchmakerRepo.putUsers(updatedUsers)
            matchmakerRepo.removeUsersByNames(formedGroup.map { it.name })
        }
    }

    override fun match(context: ChainContext, env: IKonveyorEnvironment): Boolean {
        return context.chainStatus == ChainStatus.RUNNING
    }
}