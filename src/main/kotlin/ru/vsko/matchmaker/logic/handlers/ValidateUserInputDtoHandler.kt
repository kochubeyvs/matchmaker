package ru.vsko.matchmaker.logic.handlers

import codes.spectrum.konveyor.IKonveyorEnvironment
import codes.spectrum.konveyor.IKonveyorHandler
import org.valiktor.ConstraintViolationException
import org.valiktor.functions.*
import org.valiktor.validate
import ru.vsko.matchmaker.extensions.addError
import ru.vsko.matchmaker.logic.ChainStatus
import ru.vsko.matchmaker.logic.ChainContext
import ru.vsko.matchmaker.mappings.toUserInner
import ru.vsko.matchmaker.models.dto.input.UserInputDto


object ValidateUserInputDtoHandler : IKonveyorHandler<ChainContext> {

    override suspend fun exec(context: ChainContext, env: IKonveyorEnvironment) {
        with(context) {
            try {
                validate(userInputDto) {
                    validate(UserInputDto::name).isNotNull().matches("^\\w{3,20}$".toRegex())
                    validate(UserInputDto::skill).isNotNull().matches("^\\d+(\\.\\d+)?$".toRegex())
                    validate(UserInputDto::latency).isNotNull().matches("^\\d+(\\.\\d+)?$".toRegex())
                }
                newUser = toUserInner()
            } catch (ex: ConstraintViolationException) {
                ex.constraintViolations.forEach {
                    addError(
                        message = "User validation error in field: ${it.property}",
                        ex = ex
                    )
                }
                chainStatus = ChainStatus.FAILING
            }
        }
    }

    override fun match(context: ChainContext, env: IKonveyorEnvironment): Boolean {
        return context.chainStatus == ChainStatus.RUNNING
    }
}