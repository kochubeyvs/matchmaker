package ru.vsko.matchmaker.logic.handlers

import codes.spectrum.konveyor.IKonveyorEnvironment
import codes.spectrum.konveyor.IKonveyorHandler
import io.ktor.http.*
import ru.vsko.matchmaker.logic.ChainContext
import ru.vsko.matchmaker.logic.ChainErrorLevel
import ru.vsko.matchmaker.mappings.toOutput
import ru.vsko.matchmaker.models.dto.output.HttpResponse
import ru.vsko.matchmaker.models.dto.output.ResponseData

object PrepareResponseHandler : IKonveyorHandler<ChainContext> {

    override suspend fun exec(context: ChainContext, env: IKonveyorEnvironment) {
        with(context) {
            httpResponse = when {
                chainErrors.any { it.level == ChainErrorLevel.ERROR } -> HttpResponse(
                    status = HttpStatusCode.BadRequest,
                    data = ResponseData(
                        message = "User input data is not valid",
                        errors = chainErrors.map { it.toOutput() }
                    )
                )
                else -> HttpResponse(HttpStatusCode.OK)
            }
        }
    }

    override fun match(context: ChainContext, env: IKonveyorEnvironment): Boolean {
        return true
    }
}