package ru.vsko.matchmaker.logic.handlers

import codes.spectrum.konveyor.IKonveyorEnvironment
import codes.spectrum.konveyor.IKonveyorHandler
import ru.vsko.matchmaker.logic.ChainStatus
import ru.vsko.matchmaker.logic.ChainContext
import java.time.Duration
import java.time.Instant


object OutputGroupHandler : IKonveyorHandler<ChainContext> {

    override suspend fun exec(context: ChainContext, env: IKonveyorEnvironment) {
        with(context) {
            val now = Instant.now()
            val timeSpentMs = Duration.between(startTime, now).toMillis()
            if (formedGroup.isEmpty()) {
                println("Not enough suitable users to form group, handled in $timeSpentMs ms")
            } else {
                var minSkill = Double.MAX_VALUE
                var sumSkill = 0.0
                var maxSkill = Double.MIN_VALUE

                var minLatency = Double.MAX_VALUE
                var sumLatency = 0.0
                var maxLatency = Double.MIN_VALUE

                var minDurationInPool = Duration.between(formedGroup.first().joinPoolTime, now)
                var sumDurationInPool = Duration.ZERO
                var maxDurationInPool = Duration.between(formedGroup.first().joinPoolTime, now)

                val groupSize = formedGroup.size

                formedGroup.forEach { user ->
                    if (user.skill < minSkill) minSkill = user.skill
                    sumSkill += user.skill
                    if (user.skill > maxSkill) maxSkill = user.skill

                    if (user.latency < minLatency) minLatency = user.latency
                    sumLatency += user.latency
                    if (user.latency > maxLatency) maxLatency = user.latency

                    val durationInPool = Duration.between(user.joinPoolTime, now)
                    if (durationInPool < minDurationInPool) minDurationInPool = durationInPool
                    sumDurationInPool += durationInPool
                    if (durationInPool > maxDurationInPool) maxDurationInPool = durationInPool
                }

                println("########### Group №${matchmakerRepo.getGroupNum()} formed, handled in $timeSpentMs ms ###########")
                println("Skill: min=$minSkill, max=$maxSkill, avg=${sumSkill / groupSize}")
                println("Latency: min=$minLatency, max=$maxLatency, avg=${sumLatency / groupSize}")
                println("Duration in pool: min=${minDurationInPool.toMillis()}ms, max=${maxDurationInPool.toMillis()}ms, avg=${sumDurationInPool.toMillis() / groupSize}ms")
                println("User names: ${formedGroup.joinToString(prefix = "", postfix = "") { it.name }}")
                println("************************** Detailed info **************************")
                println(formedGroup.joinToString(prefix = "", separator = "\n", postfix = ""))
                println("###################################################################")
            }
        }
    }

    override fun match(context: ChainContext, env: IKonveyorEnvironment): Boolean {
        return context.chainStatus == ChainStatus.RUNNING
    }
}