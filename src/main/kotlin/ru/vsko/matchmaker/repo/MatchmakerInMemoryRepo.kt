package ru.vsko.matchmaker.repo

import ru.vsko.matchmaker.models.inner.UserInner
import java.util.concurrent.atomic.AtomicInteger

class MatchmakerInMemoryRepo : IMatchmakerRepo {

    private val groupNum = AtomicInteger()
    private val userPool = mutableMapOf<String, UserInner>()

    override fun getGroupNum() = groupNum.incrementAndGet()

    override fun putUser(user: UserInner) {
        userPool[user.name] = user
    }

    override fun putUsers(users: List<UserInner>) {
        users.forEach { user -> userPool[user.name] = user }
    }

    override fun getUsersSortedByJoinTime(): List<UserInner> =
        userPool.values.sortedBy { it.joinPoolTime }.toList()


    override fun removeUserByName(name: String) {
        userPool.remove(name)
    }

    override fun removeUsersByNames(names: List<String>) {
        names.forEach { removeUserByName(it) }
    }
}