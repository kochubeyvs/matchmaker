package ru.vsko.matchmaker.repo

import ru.vsko.matchmaker.models.inner.UserInner

interface IMatchmakerRepo {

    fun getGroupNum(): Int

    fun putUser(user: UserInner)

    fun putUsers(users: List<UserInner>)

    fun getUsersSortedByJoinTime(): List<UserInner>

    fun removeUserByName(name: String)

    fun removeUsersByNames(names: List<String>)
}