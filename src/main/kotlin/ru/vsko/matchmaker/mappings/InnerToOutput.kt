package ru.vsko.matchmaker.mappings

import ru.vsko.matchmaker.logic.ChainError
import ru.vsko.matchmaker.logic.ChainErrorLevel
import ru.vsko.matchmaker.models.dto.output.ResponseError
import ru.vsko.matchmaker.models.dto.output.ResponseErrorLevel

fun ChainError.toOutput() = ResponseError(
    message = message,
    level = level.toOutput()
)

fun ChainErrorLevel.toOutput() = when (this) {
    ChainErrorLevel.INFO -> ResponseErrorLevel.INFO
    ChainErrorLevel.WARNING -> ResponseErrorLevel.WARNING
    ChainErrorLevel.ERROR -> ResponseErrorLevel.ERROR
}