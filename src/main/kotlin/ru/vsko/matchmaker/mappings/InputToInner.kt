package ru.vsko.matchmaker.mappings

import ru.vsko.matchmaker.logic.ChainContext
import ru.vsko.matchmaker.models.inner.UserInner

fun ChainContext.toUserInner() = UserInner(
    name = userInputDto.name ?: "",
    skill = userInputDto.skill?.toDouble() ?: Double.MIN_VALUE,
    latency = userInputDto.latency?.toDouble() ?: Double.MIN_VALUE,
    joinPoolTime = startTime,
    helpingPower = 1
)